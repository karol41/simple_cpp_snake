# Basic snake game
C/C++ written snake game for the macOS terminal using threads.

# How to launch
Inside the folder execute the following command: 
~~~~~
cmake . && make && ./bin/snake
~~~~~
# Controls
WASD - for movement  
x - to exit
# The Game
![Alt text](/snake_game.png?raw=true "Screenshot")
