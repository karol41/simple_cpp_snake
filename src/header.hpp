using namespace std;

struct Snake {

  bool gameOver;
  int widtH;
  int heighT;

  int snake_coords[2];
  int fruit_coords[2];
  int score;
  int tailX[100];
  int tailY[100];
  int nTail;
  char move;
  int speedup;

  enum eDirection { STOP = 0, LEFT, RIGHT, UP, DOWN };
  eDirection dir, current_dir;
  Snake(const int width, const int height)
      : gameOver(false), widtH(width), heighT(height),score(0),nTail(0),move('0'),speedup(0), dir(STOP) {
    snake_coords[0] = widtH / 2;
    snake_coords[1] = heighT / 2;
    fruit_coords[0] = rand() % widtH;
    fruit_coords[1] = rand() % heighT;

    }

    void Run() {
      input_enter_off();
      while (!this->gameOver) {
        this->Draw();
        // this->move = getchar();
        this->Input();
        this->Logic();
        int sleep_for = 100000 - this->speedup * 10000;
        usleep(sleep_for);
        // Sleep(10);
        // game1.gameOver = true;
      }
    }
    void Draw() { 
      
      system("clear");
      cout << "Score: " << score << "\n";
      cout << "Snake Coords: " << snake_coords[0] << " " << snake_coords[1]  << "\n";
      cout << "Fruit Coords: " << fruit_coords[0] << " " << fruit_coords[1]  << "\n";

      for (int i = 0; i < widtH + 2; ++i) {
        std::cout << "@";
      }
      std::cout << "\n";


        for (int i = 0; i <= heighT; ++i){
          for (int j = 0; j < widtH; ++j){
            if(j==0)
              std::cout << "@";
              // BLANK SPACE

              if (i == snake_coords[1] && j == snake_coords[0])
                std::cout << "O";
              else if (i == fruit_coords[1] && j == fruit_coords[0])   
                std::cout << "F";

              else {
                bool print_segment = false;
                for (int k = 0; k < nTail; k++) {
                  if (tailX[k] == j && tailY[k] == i) {
                  cout << "o";
                  print_segment = true;
                  }
                }
               if(!print_segment)
                std::cout << " ";  
              }
            if(j == widtH - 1){
              std::cout << "@";
            }
          }
          std::cout << "\n";
        }

        for (int i = 0; i < widtH + 2; ++i) {
          std::cout << "@";
      }
      std::cout << "\n";


    }

    void Input() {
      
      dir = current_dir;
      getDir(move);
    }

    void Logic() {

      int previous_tail_coords[2] = {tailX[0], tailY[0]};
      int previous_snake_coords[2];

      for (int i = 0; i < nTail; i++){
        previous_snake_coords[0] = tailX[i];
        previous_snake_coords[1] = tailY[i];
        tailX[i] = previous_tail_coords[0];
        tailY[i] = previous_tail_coords[1];
        previous_tail_coords[0] = previous_snake_coords[0];
        previous_tail_coords[1] = previous_snake_coords[1];
      }
        tailX[0] = snake_coords[0];
        tailY[0] = snake_coords[1];
      switch (dir) {
      case LEFT:
        snake_coords[0]--;
        break;
      case RIGHT:
        snake_coords[0]++;
        break;
      case DOWN:
        snake_coords[1]++;
        break;
      case UP:
        snake_coords[1]--;
        break;
      default:
        break;
        }

      if (snake_coords[0] >= widtH || snake_coords[0] < 0 || snake_coords[1] > heighT || snake_coords[1] < 0  ){
        gameOver = true;
      }

      for (int i = 0; i < nTail; i++){
        if (tailX[i] == snake_coords[0] && tailY[i] == snake_coords[1])
        gameOver = true;
      }
        if (snake_coords[0] == fruit_coords[0] &&
            snake_coords[1] == fruit_coords[1]) {
          score += 10;
          fruit_coords[0] = rand() % widtH;
          fruit_coords[1] = rand() % heighT;
          nTail++;
          //DIFFICULTY INCREASE EVERY 20 points
          if(score != 0 && score % 20 == 0) speedup++;

        }
     
    }

    void getDir(char pressed_key){
         switch (pressed_key){
          case 'a':
            dir = LEFT;
            break;
          case 's':
            dir = DOWN;
            break;
          case 'd':
            dir = RIGHT;
            break;
          case 'w':
            dir = UP;
            break;
          case 'x':
            this->gameOver = true;
            break;
          default :
            // dir = current_dir;
            break;
          }
    }
};
