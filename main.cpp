#include <iostream>
#include <stdlib.h> 
// #include <ncurses.h> 
#include <termios.h>
#include <unistd.h>
#include <string>
#include <stdio.h>
#include "src/input.h"
#include "src/header.hpp"
#include <thread>

using namespace std;

int main() {

  Snake game1(50  , 20);
  
  auto run = [&game1]() { game1.Run(); };

  std::thread worker(run);
  while (!game1.gameOver) game1.move = getchar();
  worker.join();

  cout << "\n GAME OVER \n";

  return 0;
}